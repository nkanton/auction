package com.nkanton.auction.utility.notification;

import com.nkanton.auction.model.User;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class NotificationServiceImpl implements NotificationService {

    private JavaMailSender javaMailSender;

    public NotificationServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    @Override
    public void notifyParticipants(Collection<User> participants, String message) {
        for (User participant : participants) {
            String email = participant.getEmail();
            sendMessage(email, message);
        }
    }


    public void sendMessage(String email, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("noreply@auction.com");
        mailMessage.setTo(email);
        mailMessage.setSubject("Notification");
        mailMessage.setText(message);

//        javaMailSender.send(mailMessage);

        System.out.println("User with email: " + email + " has been notified.");
    }
}
