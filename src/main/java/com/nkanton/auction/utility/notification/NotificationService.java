package com.nkanton.auction.utility.notification;

import com.nkanton.auction.model.User;

import java.util.Collection;

public interface NotificationService {
    void notifyParticipants(Collection<User> participants, String message);
}
