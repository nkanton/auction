package com.nkanton.auction.utility.security;

import com.nkanton.auction.exception.InvalidRequestException;
import com.nkanton.auction.exception.user.UserNotFoundException;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * The Security service. Simply checks the identity of the user.
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    private HttpServletRequest request;

    private UserRepository userRepository;

    /**
     * Instantiates a new Security service.
     *
     * @param request        the request
     * @param userRepository
     */
    public SecurityServiceImpl(HttpServletRequest request, UserRepository userRepository) {
        this.request = request;
        this.userRepository = userRepository;
    }

    /**
     * Checks if the header with the email property exists and if the user with provided email is in Database.
     * throw InvalidRequestException
     *
     * @return the identity or throw
     * @throws InvalidRequestException if
     *                                 header with key "email" hasn't been found.
     * @throws UserNotFoundException   if
     *                                 user with provided email hasn't been found.
     */
    @Override
    public User getIdentityOrThrow() {
        String email = request.getHeader("email");
        if (email == null)
            throw new InvalidRequestException("Please, set header of the request with you email address");

        User user = userRepository.findByEmail(email);
        if (user == null)
            throw new UserNotFoundException();

        return user;
    }
}
