package com.nkanton.auction.utility.security;

import com.nkanton.auction.model.User;

public interface SecurityService {
    User getIdentityOrThrow();
}
