package com.nkanton.auction.utility;

import com.nkanton.auction.model.Auction;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

public class Utility {

    private Utility() {
    }

    public static boolean isAuctionInProgress(Auction auction) {
        if (auction == null) throw new InvalidParameterException("Auction can't be null");

        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.isAfter(auction.getStart()) && currentTime.isBefore(auction.getEnd());
    }

    public static boolean beforeAuction
            (Auction auction) {
        if (auction == null) return false;

        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.isBefore(auction.getStart());
    }
}
