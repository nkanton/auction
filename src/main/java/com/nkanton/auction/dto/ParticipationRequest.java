package com.nkanton.auction.dto;

import javax.validation.constraints.NotNull;

public class ParticipationRequest {
    @NotNull
    private Long auctionId;

    public ParticipationRequest() {
    }

    public ParticipationRequest(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }
}
