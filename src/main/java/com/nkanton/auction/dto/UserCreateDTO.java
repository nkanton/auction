package com.nkanton.auction.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserCreateDTO {
    @Email
    private String email;

    @NotEmpty
    @NotNull(message = "Name can't be null")
    private String name;

    public UserCreateDTO() {
    }

    public UserCreateDTO(@Email String email, @NotEmpty @NotNull String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
