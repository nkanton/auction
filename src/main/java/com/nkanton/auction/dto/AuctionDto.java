package com.nkanton.auction.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class AuctionDto {

    private Long id;

    @NotEmpty
    @NotNull(message = "Name can't be null")
    private String name;

    @NotNull(message = "Start time can't be null")
    private LocalDateTime start;

    @NotNull(message = "End time can't be null")
    private LocalDateTime end;

    public AuctionDto() {
    }

    public AuctionDto(String name, LocalDateTime start, LocalDateTime end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
