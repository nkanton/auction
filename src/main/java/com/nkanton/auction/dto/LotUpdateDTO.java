package com.nkanton.auction.dto;


import java.math.BigDecimal;

public class LotUpdateDTO extends LotCreateDTO {
    private Long id;

    public LotUpdateDTO() {
    }

    public LotUpdateDTO(Long id, BigDecimal initialPrice, String name, String description, BigDecimal instantBuyPrice, Long auction) {
        super(initialPrice, name, description, instantBuyPrice, auction);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
