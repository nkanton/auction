package com.nkanton.auction.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class BidDTO {
    @Positive
    @NotNull
    private Long id;

    @Positive
    @NotNull
    private BigDecimal amount;

    @Positive
    @NotNull
    private Long lot;

    @Positive
    @NotNull
    private Long user;

    public BidDTO() {
    }

    public BidDTO(Long id, BigDecimal amount, Long lot, Long user) {
        this.id = id;
        this.amount = amount;
        this.lot = lot;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getLot() {
        return lot;
    }

    public void setLot(Long lot) {
        this.lot = lot;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }
}
