package com.nkanton.auction.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class LotCreateDTO {
    @Positive(message = "Price can't be negative")
    @NotNull(message = "Price can't be null")
    private BigDecimal initialPrice;

    @NotNull(message = "Name can't be null")
    private String name;

    @Null
    private String description;

    @Positive(message = "Price can't be negative")
    @NotNull(message = "Price can't be null")
    private BigDecimal instantBuyPrice;

    private Long auction;

    public LotCreateDTO() {
    }

    public LotCreateDTO(BigDecimal initialPrice, String name, String description, BigDecimal instantBuyPrice, Long auction) {
        this.initialPrice = initialPrice;
        this.name = name;
        this.description = description;
        this.instantBuyPrice = instantBuyPrice;
        this.auction = auction;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getInstantBuyPrice() {
        return instantBuyPrice;
    }

    public void setInstantBuyPrice(BigDecimal instantBuyPrice) {
        this.instantBuyPrice = instantBuyPrice;
    }

    public Long getAuction() {
        return auction;
    }

    public void setAuction(Long auction) {
        this.auction = auction;
    }
}
