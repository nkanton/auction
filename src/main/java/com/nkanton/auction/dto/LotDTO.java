package com.nkanton.auction.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class LotDTO {
    private Long id;

    @Positive
    private BigDecimal initialPrice;

    @NotNull
    @NotEmpty
    private String name;

    private String description;

    private Long owner;

    private Long auction;

    private Long buyer;

    private BigDecimal instantBuyPrice;

    private BigDecimal finalPrice;

    public LotDTO() {
    }

    public LotDTO(Long id, BigDecimal initialPrice, String name, String description, Long owner, Long auction, Long buyer, BigDecimal instantBuyPrice, BigDecimal finalPrice) {
        this.id = id;
        this.initialPrice = initialPrice;
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.auction = auction;
        this.buyer = buyer;
        this.instantBuyPrice = instantBuyPrice;
        this.finalPrice = finalPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public BigDecimal getInstantBuyPrice() {
        return instantBuyPrice;
    }

    public void setInstantBuyPrice(BigDecimal instantBuyPrice) {
        this.instantBuyPrice = instantBuyPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Long getAuction() {
        return auction;
    }

    public void setAuction(Long auction) {
        this.auction = auction;
    }

    public Long getBuyer() {
        return buyer;
    }

    public void setBuyer(Long buyer) {
        this.buyer = buyer;
    }
}
