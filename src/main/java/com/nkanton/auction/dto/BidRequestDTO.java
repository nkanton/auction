package com.nkanton.auction.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class BidRequestDTO {
    @Positive
    @NotNull
    private BigDecimal amount;

    @Positive
    @NotNull
    private Long lot;

    public BidRequestDTO() {
    }

    public BidRequestDTO(BigDecimal amount, Long lot) {
        this.amount = amount;
        this.lot = lot;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getLot() {
        return lot;
    }

    public void setLot(Long lot) {
        this.lot = lot;
    }
}
