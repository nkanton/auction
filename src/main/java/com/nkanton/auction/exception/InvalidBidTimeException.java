package com.nkanton.auction.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidBidTimeException extends RuntimeException {
    public InvalidBidTimeException(String message) {
        super(message);
    }
}
