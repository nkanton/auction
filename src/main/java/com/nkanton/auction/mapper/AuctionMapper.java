package com.nkanton.auction.mapper;

import com.nkanton.auction.dto.AuctionCreateDto;
import com.nkanton.auction.dto.AuctionDto;
import com.nkanton.auction.model.Auction;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuctionMapper {
    Auction fromDto(AuctionCreateDto auctionCreateDto);

    AuctionDto toDto(Auction auction);
}
