package com.nkanton.auction.mapper;

import com.nkanton.auction.dto.BidDTO;
import com.nkanton.auction.dto.BidRequestDTO;
import com.nkanton.auction.model.Bid;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = LotMapper.class)
public interface BidMapper {

    @Mapping(target = "lot.id", source = "lot")
    Bid fromDto(BidRequestDTO bidRequestDTO);

    @Mapping(target = "lot", source = "lot.id")
    List<BidDTO> toDto(List<Bid> bids);

    @Mapping(target = "lot", source = "lot.id")
    BidDTO toDto(Bid bid);
}
