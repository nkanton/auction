package com.nkanton.auction.mapper;

import com.nkanton.auction.dto.LotCreateDTO;
import com.nkanton.auction.dto.LotDTO;
import com.nkanton.auction.model.Lot;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, AuctionMapper.class})
public interface LotMapper {
    @Mapping(target = "auction.id", source = "auction")
    Lot fromDto(LotCreateDTO lotDTO);

    @Mapping(target = "buyer", source = "buyer.id")
    @Mapping(target = "owner", source = "owner.id")
    @Mapping(target = "auction", source = "auction.id")
    List<LotDTO> toDto(List<Lot> lots);

    @Mapping(target = "buyer.id", source = "buyer")
    @Mapping(target = "owner.id", source = "owner")
    @Mapping(target = "auction.id", source = "auction")
    Lot fromDto(LotDTO lotDTO);

    @Mapping(target = "buyer", source = "buyer.id")
    @Mapping(target = "owner", source = "owner.id")
    @Mapping(target = "auction", source = "auction.id")
    LotDTO toDto(Lot lot);
}
