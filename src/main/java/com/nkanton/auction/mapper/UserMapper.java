package com.nkanton.auction.mapper;

import com.nkanton.auction.dto.UserCreateDTO;
import com.nkanton.auction.dto.UserDTO;
import com.nkanton.auction.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = LotMapper.class)
public interface UserMapper {
    User fromDto(UserDTO userDTO);

    UserDTO toDto(User user);

    User fromDto(UserCreateDTO userDTO);
}
