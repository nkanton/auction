package com.nkanton.auction.controller;

import com.nkanton.auction.dto.AuctionCreateDto;
import com.nkanton.auction.dto.AuctionDto;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.service.AuctionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/auction")
public class AuctionController {

    private AuctionService auctionService;

    public AuctionController(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Long createAuction(@Valid @RequestBody AuctionCreateDto auctionCreateDto) {
        return auctionService.create(auctionCreateDto);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public AuctionDto getAuction(@PathVariable Long id) {
        return auctionService.getById(id);
    }

    @GetMapping("/{id}/lots")
    @ResponseBody
    public Set<Lot> getAllLots(@PathVariable Long auction_id) {
        return auctionService.getByLotId(auction_id);
    }
}
