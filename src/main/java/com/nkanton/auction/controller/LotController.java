package com.nkanton.auction.controller;

import com.nkanton.auction.dto.LotCreateDTO;
import com.nkanton.auction.dto.LotDTO;
import com.nkanton.auction.dto.LotUpdateDTO;
import com.nkanton.auction.service.LotService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/lots")
public class LotController {

    private LotService lotService;

    public LotController(LotService lotService) {
        this.lotService = lotService;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Long createLot(@Valid @RequestBody LotCreateDTO lotCreateDTO) {
        return lotService.create(lotCreateDTO);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public LotDTO getLot(@PathVariable Long id) {
        return lotService.get(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteLotById(@PathVariable Long id) {
        lotService.delete(id);
    }

    @GetMapping("/owner/{id}")
    @ResponseBody
    public List<LotDTO> getLotsByOwnerId(@PathVariable Long id) {
        return lotService.getByOwner(id);
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    public void updateLot(@Valid @RequestBody LotUpdateDTO lotDTO) {
        lotService.update(lotDTO);
    }

    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void instantBuy(@PathVariable Long id) {
        lotService.instantBuy(id);
    }
}
