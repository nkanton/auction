package com.nkanton.auction.controller;

import com.nkanton.auction.dto.BidDTO;
import com.nkanton.auction.dto.BidRequestDTO;
import com.nkanton.auction.service.BidService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/bids")
public class BidController {

    private BidService bidService;

    public BidController(BidService bidService) {
        this.bidService = bidService;
    }

    @GetMapping("/lot/{id}")
    @ResponseBody
    public List<BidDTO> getAllBidsByLotId(@PathVariable Long id) {
        return bidService.getBids(id);
    }

    @PostMapping
    @ResponseBody
    public Long placeBid(@Valid @RequestBody BidRequestDTO bidRequestDTO) {
        return bidService.placeBid(bidRequestDTO);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public BidDTO getBid(@PathVariable Long id) {
        return bidService.getBid(id);
    }
}
