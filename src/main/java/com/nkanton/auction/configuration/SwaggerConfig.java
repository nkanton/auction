package com.nkanton.auction.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .globalRequestParameters(Arrays.asList(new RequestParameterBuilder()
                        .description("User identifier")
                        .name("email")
                        .in(ParameterType.HEADER)
                        .required(true)
                        .build()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.nkanton.auction.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}