package com.nkanton.auction.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String name;

    @OneToMany(mappedBy = "owner")
    private Set<Lot> lotsToSell;

    @OneToMany(mappedBy = "buyer")
    private Set<Lot> lotsBought;

    @OneToMany(mappedBy = "bidder")
    private Set<Bid> bids;

    public User() {
    }

    public User(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Lot> getLotsToSell() {
        return lotsToSell;
    }

    public void setLotsToSell(Set<Lot> lotsToSell) {
        this.lotsToSell = lotsToSell;
    }

    public Set<Lot> getLotsBought() {
        return lotsBought;
    }

    public void setLotsBought(Set<Lot> lotsBought) {
        this.lotsBought = lotsBought;
    }

    public Set<Bid> getBids() {
        return bids;
    }

    public void setBids(Set<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
