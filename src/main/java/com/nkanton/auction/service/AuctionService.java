package com.nkanton.auction.service;

import com.nkanton.auction.dto.AuctionCreateDto;
import com.nkanton.auction.dto.AuctionDto;
import com.nkanton.auction.model.Lot;

import java.util.Set;

public interface AuctionService {
    Long create(AuctionCreateDto auctionCreateDto);

    AuctionDto getById(Long id);

    Set<Lot> getByLotId(Long auction_id);
}
