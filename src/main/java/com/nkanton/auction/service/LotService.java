package com.nkanton.auction.service;

import com.nkanton.auction.dto.LotCreateDTO;
import com.nkanton.auction.dto.LotDTO;
import com.nkanton.auction.dto.LotUpdateDTO;

import java.util.List;

public interface LotService {
    Long create(LotCreateDTO lotDTO);

    void update(LotUpdateDTO lotDTO);

    List<LotDTO> getByOwner(Long email);

    LotDTO get(Long id);

    void delete(Long id);

    void instantBuy(Long lotId);
}
