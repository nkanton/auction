package com.nkanton.auction.service;

import com.nkanton.auction.dto.LotCreateDTO;
import com.nkanton.auction.dto.LotDTO;
import com.nkanton.auction.dto.LotUpdateDTO;
import com.nkanton.auction.exception.ForbiddenActionException;
import com.nkanton.auction.exception.ItemNotFoundException;
import com.nkanton.auction.mapper.LotMapper;
import com.nkanton.auction.model.Bid;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.LotRepository;
import com.nkanton.auction.utility.notification.NotificationService;
import com.nkanton.auction.utility.security.SecurityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nkanton.auction.utility.Utility.beforeAuction;
import static com.nkanton.auction.utility.Utility.isAuctionInProgress;

@Transactional
@Service
public class LotServiceImpl implements LotService {
    public final static String LOT_HASNT_BEEN_FOUND = "Lot hasn't been found";
    public final static String LOW_PRICE = "New price can't be lower or equal current price.";
    public final static String LOT_CLOSED = "Lot has already been closed.";

    private SecurityService securityService;

    private NotificationService notificationService;

    private LotRepository lotRepository;

    private LotMapper lotMapper;

    public LotServiceImpl(SecurityService securityService, NotificationService notificationService, LotRepository lotRepository, LotMapper lotMapper) {
        this.securityService = securityService;
        this.notificationService = notificationService;
        this.lotRepository = lotRepository;
        this.lotMapper = lotMapper;
    }

    @Override
    public Long create(LotCreateDTO lotDTO) {
        User user = securityService.getIdentityOrThrow();

        Lot lot = lotMapper.fromDto(lotDTO);
        lot.setOwner(user);
        lot.setFinalPrice(lot.getInstantBuyPrice());

        lotRepository.save(lot);
        return lot.getId();
    }

    @Override
    public void update(LotUpdateDTO lotDTO) {
        User user = securityService.getIdentityOrThrow();

        Optional<Lot> optionalLot = lotRepository.findById(lotDTO.getId());
        if (optionalLot.isPresent()) {
            Lot lot = optionalLot.get();

            if (!Objects.equals(lot.getOwner(), user)) {
                throw new ForbiddenActionException("Only owners can update their lot");
            }

            boolean changed = checkIfChanged(lot, lotDTO);
            if (changed) {
                if (beforeAuction(lot.getAuction())) {
                    lot.setName(lotDTO.getName());
                    lot.setInitialPrice(lotDTO.getInitialPrice());
                    lot.setDescription(lotDTO.getDescription());
                    lotRepository.save(lot);

                    List<User> users = lot.getBids().stream().map(Bid::getBidder).collect(Collectors.toList());

                    notificationService.notifyParticipants(users, "Lot " + lot.getId() + " has been update.");
                } else {
                    throw new ForbiddenActionException("The lot can't be updated after the auction started.");
                }
            }
        } else {
            throw new ItemNotFoundException(LOT_HASNT_BEEN_FOUND);
        }
    }

    private boolean checkIfChanged(Lot lot, LotUpdateDTO lotDTO) {
        return !Objects.equals(lot.getName(), lotDTO.getName()) ||
                !Objects.equals(lot.getDescription(), lotDTO.getDescription()) ||
                !Objects.equals(lot.getInitialPrice(), lotDTO.getInitialPrice());
    }

    @Override
    public List<LotDTO> getByOwner(Long id) {
        List<Lot> byOwner = lotRepository.findByOwnerId(id);
        return lotMapper.toDto(byOwner);
    }

    @Override
    public LotDTO get(Long id) {
        Optional<Lot> optionalLot = lotRepository.findById(id);
        if (optionalLot.isPresent()) {
            return lotMapper.toDto(optionalLot.get());
        } else {
            throw new ItemNotFoundException(LOT_HASNT_BEEN_FOUND);
        }
    }

    @Override
    public void delete(Long id) {
        User user = securityService.getIdentityOrThrow();

        Optional<Lot> lotOptional = lotRepository.findById(id);
        if (lotOptional.isPresent()) {
            Lot lot = lotOptional.get();

            if (!beforeAuction(lot.getAuction())) {
                throw new ForbiddenActionException("The lot can be deleted only before the auction has been started.");
            }

            if (!Objects.equals(lot.getOwner(), user)) {
                throw new ForbiddenActionException("Only owners can delete their lot");
            }

            lotRepository.deleteById(id);
        } else {
            throw new ItemNotFoundException(LOT_HASNT_BEEN_FOUND);
        }
    }

    @Override
    public void instantBuy(Long lotId) {
        User requestUser = securityService.getIdentityOrThrow();

        Optional<Lot> optionalLot = lotRepository.findById(lotId);
        if (optionalLot.isPresent()) {
            Lot lot = optionalLot.get();

            if (Objects.equals(lot.getOwner().getId(), requestUser.getId())) {
                throw new ForbiddenActionException("Owner can't buy own lot.");
            }

            if (isAuctionInProgress(lot.getAuction())) {
                throw new ForbiddenActionException("Instant buy can't be performed during active hours of an auction");
            }
            lot.setFinalPrice(lot.getInstantBuyPrice());
            lotRepository.save(lot);

            List<User> users = lot.getBids().stream().map(Bid::getBidder).collect(Collectors.toList());
            notificationService.notifyParticipants(users, "Lot " + lot.getId() + " has been sold.");
        } else {
            throw new ItemNotFoundException(LOT_HASNT_BEEN_FOUND);
        }
    }
}
