package com.nkanton.auction.service;

import com.nkanton.auction.dto.AuctionCreateDto;
import com.nkanton.auction.dto.AuctionDto;
import com.nkanton.auction.exception.ItemNotFoundException;
import com.nkanton.auction.mapper.AuctionMapper;
import com.nkanton.auction.model.Auction;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.repository.AuctionRepository;
import com.nkanton.auction.utility.security.SecurityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Transactional
@Service
public class AuctionServiceImpl implements AuctionService {
    private SecurityService securityService;

    private AuctionRepository auctionRepository;

    private AuctionMapper auctionMapper;

    public AuctionServiceImpl(SecurityService securityService, AuctionRepository auctionRepository, AuctionMapper auctionMapper) {
        this.securityService = securityService;
        this.auctionRepository = auctionRepository;
        this.auctionMapper = auctionMapper;
    }

    @Override
    public Long create(AuctionCreateDto auctionCreateDto) {
        Auction auction = auctionMapper.fromDto(auctionCreateDto);
        auctionRepository.save(auction);
        return auction.getId();
    }

    @Override
    public AuctionDto getById(Long id) {
        Optional<Auction> optionalAuction = auctionRepository.findById(id);
        if (optionalAuction.isPresent()) {
            return auctionMapper.toDto(optionalAuction.get());
        } else {
            throw new ItemNotFoundException("Auction hasn't been found");
        }
    }

    @Override
    public Set<Lot> getByLotId(Long lotId) {
        Optional<Auction> auctionOptional = auctionRepository.findById(lotId);
        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();
            return auction.getLots();
        } else {
            throw new ItemNotFoundException("Auction hasn't been found");
        }
    }
}
