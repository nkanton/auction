package com.nkanton.auction.service;

import com.nkanton.auction.dto.UserCreateDTO;
import com.nkanton.auction.dto.UserDTO;

public interface UserService {
    Long create(UserCreateDTO userDTO);

    UserDTO getByEmail(String email);

    UserDTO getById(Long id);
}
