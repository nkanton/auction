package com.nkanton.auction.service;

import com.nkanton.auction.dto.BidDTO;
import com.nkanton.auction.dto.BidRequestDTO;

import java.util.List;

public interface BidService {
    BidDTO getBid(Long id);

    List<BidDTO> getBids(Long id);

    Long placeBid(BidRequestDTO lotId);
}
