package com.nkanton.auction.service;

import com.nkanton.auction.dto.UserCreateDTO;
import com.nkanton.auction.dto.UserDTO;
import com.nkanton.auction.exception.user.UserAlreadyExists;
import com.nkanton.auction.exception.user.UserNotFoundException;
import com.nkanton.auction.mapper.UserMapper;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public Long create(UserCreateDTO userDTO) {
        User userFromDb = userRepository.findByEmail(userDTO.getEmail());

        if (userFromDb == null) {
            User newUser = userMapper.fromDto(userDTO);
            userRepository.save(newUser);
            return newUser.getId();
        } else {
            throw new UserAlreadyExists();
        }
    }

    @Override
    public UserDTO getByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return userMapper.toDto(user);
    }

    @Override
    public UserDTO getById(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return userMapper.toDto(user);
        } else {
            throw new UserNotFoundException();
        }
    }
}
