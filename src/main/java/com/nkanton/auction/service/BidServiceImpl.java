package com.nkanton.auction.service;

import com.nkanton.auction.dto.BidDTO;
import com.nkanton.auction.dto.BidRequestDTO;
import com.nkanton.auction.exception.ForbiddenActionException;
import com.nkanton.auction.exception.InvalidBidTimeException;
import com.nkanton.auction.exception.InvalidRequestException;
import com.nkanton.auction.exception.ItemNotFoundException;
import com.nkanton.auction.mapper.BidMapper;
import com.nkanton.auction.model.Bid;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.BidRepository;
import com.nkanton.auction.repository.LotRepository;
import com.nkanton.auction.utility.notification.NotificationService;
import com.nkanton.auction.utility.security.SecurityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nkanton.auction.service.LotServiceImpl.LOW_PRICE;
import static com.nkanton.auction.utility.Utility.isAuctionInProgress;

@Transactional
@Service
public class BidServiceImpl implements BidService {

    private BidRepository bidRepository;

    private BidMapper bidMapper;

    private SecurityService securityService;

    private NotificationService notificationService;

    private LotRepository lotRepository;

    public BidServiceImpl(BidRepository bidRepository, BidMapper bidMapper, SecurityService securityService, NotificationService notificationService, LotRepository lotRepository) {
        this.bidRepository = bidRepository;
        this.bidMapper = bidMapper;
        this.securityService = securityService;
        this.notificationService = notificationService;
        this.lotRepository = lotRepository;
    }

    @Override
    public BidDTO getBid(Long id) {
        Optional<Bid> optionalBid = bidRepository.findById(id);
        if (optionalBid.isPresent()) {
            return bidMapper.toDto(optionalBid.get());
        } else {
            throw new ItemNotFoundException("Bid hasn't been found.");
        }
    }

    @Override
    public List<BidDTO> getBids(Long id) {
        List<Bid> bidList = bidRepository.findByLotId(id);
        return bidMapper.toDto(bidList);
    }

    @Override
    public Long placeBid(BidRequestDTO bidRequestDTO) {
        User requestUser = securityService.getIdentityOrThrow();

        Bid bid = bidMapper.fromDto(bidRequestDTO);

        Lot lot = bid.getLot();

        Optional<Lot> lotOptional = lotRepository.findById(lot.getId());
        if (lotOptional.isPresent()) {
            lot = lotOptional.get();
        } else {
            throw new ItemNotFoundException("Lot hasn't been found.");
        }

        if (Objects.equals(lot.getOwner(), requestUser)) {
            throw new ForbiddenActionException("Owners are not allowed to place bids on their lots.");
        }

        if (!isAuctionInProgress(lot.getAuction())) {
            throw new InvalidBidTimeException("Bid has been placed not in active hours of auction.");
        }

        if (lot.getInitialPrice().compareTo(bid.getAmount()) >= 0) {
            throw new InvalidRequestException(LOW_PRICE);
        } else {
            lot.setFinalPrice(bid.getAmount());
            lot.setBuyer(requestUser);
            lotRepository.save(lot);

            bid.setBidder(requestUser);
            bidRepository.save(bid);

            List<User> users = lot.getBids().stream().map(Bid::getBidder).collect(Collectors.toList());
            notificationService.notifyParticipants(users, "Lot price " + lot.getId() + " has been changed.");
        }
        return bid.getId();
    }
}
