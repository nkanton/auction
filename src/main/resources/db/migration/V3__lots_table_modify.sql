ALTER TABLE lots
ADD COLUMN buyer_id bigint,
ADD COLUMN auction_id bigint NOT NULL,
ADD COLUMN final_price numeric(19,2) NOT NULL,
ADD CONSTRAINT lots_buyer_id_fkey FOREIGN KEY (buyer_id)
        REFERENCES users (id),
ADD CONSTRAINT lots_auction_id_fkey FOREIGN KEY (auction_id)
        REFERENCES auctions (id);

ALTER TABLE lots
RENAME COLUMN price TO initial_price;


