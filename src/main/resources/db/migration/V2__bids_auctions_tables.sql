CREATE TABLE auctions
(
    id bigserial NOT NULL,
    name character varying(64),
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    CONSTRAINT auctions_pkey PRIMARY KEY (id)
);

CREATE TABLE bids
(
    id bigserial NOT NULL,
    amount numeric(19,2) NOT NULL,
    lot_id bigint NOT NULL,
    user_id bigint NOT NULL,
    CONSTRAINT bids_pkey PRIMARY KEY (id),
    CONSTRAINT bids_lot_id_fkey FOREIGN KEY (lot_id)
        REFERENCES lots (id),
    CONSTRAINT bids_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES users (id)
);

