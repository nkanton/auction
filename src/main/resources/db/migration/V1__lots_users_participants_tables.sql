CREATE TABLE users
(
    id bigserial NOT NULL,
    email character varying(254) NOT NULL,
    name character varying(64),
    CONSTRAINT "Users_pkey" PRIMARY KEY (id),
    CONSTRAINT "Users_email_key" UNIQUE (email)
);

CREATE TABLE lots
(
    id bigserial NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(254),
    price numeric(19,2) NOT NULL,
    owner_id bigint NOT NULL,
    instant_buy_price numeric(19,2) NOT NULL,
    open boolean NOT NULL DEFAULT true,
    CONSTRAINT lots_pkey PRIMARY KEY (id),
    CONSTRAINT lots_owner_id_fkey FOREIGN KEY (owner_id)
        REFERENCES users (id)
);

CREATE TABLE participants
(
    user_id bigint NOT NULL,
    lot_id bigint NOT NULL
);
