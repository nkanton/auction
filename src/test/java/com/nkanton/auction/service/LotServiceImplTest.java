package com.nkanton.auction.service;

import com.nkanton.auction.dto.LotUpdateDTO;
import com.nkanton.auction.exception.ForbiddenActionException;
import com.nkanton.auction.model.Auction;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.LotRepository;
import com.nkanton.auction.repository.UserRepository;
import com.nkanton.auction.utility.security.SecurityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LotServiceImplTest {
    @InjectMocks
    private LotServiceImpl lotService;
    @Mock
    private LotRepository lotRepository;
    @Mock
    private SecurityService securityService;

    private String header = "email";

    private String email = "bob@bob.com";

    @Test
    public void instanceBuy() {
        User owner = new User(email, "Bob");
        owner.setId(5L);

        Lot lot = new Lot(new BigDecimal(5000), "Bob", "SomeDescription", owner, new BigDecimal(10000));
        lot.setId(4L);

        when(lotRepository.findById(4L)).thenReturn(Optional.of(lot));
        when(securityService.getIdentityOrThrow()).thenReturn(owner);

        assertThrows(ForbiddenActionException.class, () -> {
            lotService.instantBuy(lot.getId());
        }, LotServiceImpl.LOT_CLOSED);
    }

    @Test
    public void invalidLotDelete() {
        User user = new User(email, "Bob");

        Lot lot = new Lot(new BigDecimal(5000), "Bob", "SomeDescription", user, new BigDecimal(10000));
        lot.setId(4L);
        LocalDateTime now = LocalDateTime.now();
        Auction carAuction = new Auction("Car auction", now, now.plus(10, ChronoUnit.HOURS));
        lot.setAuction(carAuction);

        when(lotRepository.findById(4L)).thenReturn(Optional.of(lot));
        when(securityService.getIdentityOrThrow()).thenReturn(user);

        assertThrows(ForbiddenActionException.class, () -> {
            lotService.delete(lot.getId());
        });
    }

    @Test
    public void update(){
        User user = new User(email, "Bob");

        Lot lot = new Lot(new BigDecimal(5000), "Bob", "SomeDescription", user, new BigDecimal(10000));
        LocalDateTime now = LocalDateTime.now();
        Auction carAuction = new Auction("Car auction", now, now.plus(10, ChronoUnit.HOURS));
        lot.setAuction(carAuction);
        lot.setId(1L);

        LotUpdateDTO lotDto = new LotUpdateDTO(1L, new BigDecimal(5000), "Bob", "NewDescription", new BigDecimal(10000),  carAuction.getId());

        when(lotRepository.findById(1L)).thenReturn(Optional.of(lot));
        when(securityService.getIdentityOrThrow()).thenReturn(user);

        assertThrows(ForbiddenActionException.class, () -> {
            lotService.update(lotDto);
        });
    }
}