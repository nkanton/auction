package com.nkanton.auction.controller;

import com.nkanton.auction.dto.BidRequestDTO;
import com.nkanton.auction.model.Auction;
import com.nkanton.auction.model.Bid;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.BidRepository;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class BidControllerIntegrationTest extends BaseTest {

    @Autowired
    private BidRepository bidRepository;

    @Test
    public void placeBidTest() {
        User user = getUser("Morty");

        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime endTime = currentTime.plus(2, ChronoUnit.HOURS);
        Auction auction = createAuction(currentTime, endTime, "Car auction");
        Lot newLot = createLotWithUser(auction, "Zik");
        BigDecimal bidAmount = newLot.getInitialPrice().add(new BigDecimal("100.00"));
        BidRequestDTO bidRequestDTO = new BidRequestDTO(bidAmount, newLot.getId());
        Bid bid = null;
        try {
            MvcResult mvcResult = mockMvc.perform(post("/bids")
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("email", user.getEmail())
                    .content(objectMapper.writeValueAsString(bidRequestDTO))
            ).andExpect(status().isOk()).andReturn();

            String contentAsString = mvcResult.getResponse().getContentAsString();
            Long bidId = Long.valueOf(contentAsString);
            Optional<Bid> optionalBid = bidRepository.findById(bidId);
            if(optionalBid.isPresent()){
                bid = optionalBid.get();
                Assertions.assertEquals(bid.getLot().getId(), newLot.getId());
                Assertions.assertEquals(bid.getBidder().getId(), user.getId());
                Assertions.assertEquals(bid.getAmount(), bidAmount);

                Lot lot = lotRepository.findById(newLot.getId()).get();
                Assertions.assertEquals(lot.getFinalPrice(), bidAmount);
                Assertions.assertEquals(lot.getBuyer().getId(), user.getId());
            }else {
                Assertions.fail("Bid can't be found in repository");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if(bid != null){
                bidRepository.delete(bid);
            }
            lotRepository.delete(newLot);
            userRepository.delete(newLot.getOwner());
            userRepository.delete(user);
            auctionRepository.delete(auction);
        }
    }

    @Test
    public void invalidLowPriceTest() {
        User user = getUser("Morty");

        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime endTime = currentTime.plus(2, ChronoUnit.HOURS);
        Auction auction = createAuction(currentTime, endTime, "Car auction");
        Lot newLot = createLotWithUser(auction, "Zik");
        BidRequestDTO bidRequestDTO = new BidRequestDTO(newLot.getInitialPrice().subtract(new BigDecimal(100)), newLot.getId());
        try {
            mockMvc.perform(post("/bids")
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("email", user.getEmail())
                    .content(objectMapper.writeValueAsString(bidRequestDTO))
            ).andExpect(status().isBadRequest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lotRepository.delete(newLot);
            userRepository.delete(newLot.getOwner());
            userRepository.delete(user);
            auctionRepository.delete(auction);
        }
    }
}