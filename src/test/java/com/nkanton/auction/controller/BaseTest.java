package com.nkanton.auction.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nkanton.auction.model.Auction;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import com.nkanton.auction.repository.AuctionRepository;
import com.nkanton.auction.repository.LotRepository;
import com.nkanton.auction.repository.UserRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BaseTest {
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected LotRepository lotRepository;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected AuctionRepository auctionRepository;

    protected Lot createLotWithUser(Auction auction, String userName) {
        User user = getUser(userName);
        return createLot(auction, user);
    }

    protected Lot createLot(Auction auction, User user) {
        Lot lot = new Lot(new BigDecimal("1000.00"), "car", "nice car", user, new BigDecimal("3000.00"));
        lot.setAuction(auction);
        lotRepository.save(lot);
        return lot;
    }

    protected User getUser(String name) {
        String email = name + "@" + name + ".com";
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User(email, name);
            userRepository.save(user);
        }
        return user;
    }

    protected Auction createAuction(LocalDateTime startTime, LocalDateTime endTime, String name) {
        Auction auction = new Auction(name, startTime, endTime);
        auctionRepository.save(auction);
        return auction;
    }
}
