package com.nkanton.auction.controller;

import com.nkanton.auction.dto.LotCreateDTO;
import com.nkanton.auction.model.Auction;
import com.nkanton.auction.model.Lot;
import com.nkanton.auction.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LotControllerIntegrationTest extends BaseTest {

    @Test
    public void lotCreationTest() {
        User user = getUser("Rob");

        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime endTime = currentTime.plus(2, ChronoUnit.HOURS);
        Auction auction = createAuction(currentTime, endTime, "Car auction");

        LotCreateDTO newLot = new LotCreateDTO(new BigDecimal(10000), "Car", "lalala", new BigDecimal(50000), auction.getId());

        try {
            MvcResult mvcResult = mockMvc.perform(post("/lots")
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("email", user.getEmail())
                    .content(objectMapper.writeValueAsString(newLot))
            ).andExpect(status().isCreated()).andReturn();

            String contentAsString = mvcResult.getResponse().getContentAsString();
            Long lotId = Long.valueOf(contentAsString);

            Optional<Lot> optionalLot = lotRepository.findById(lotId);
            if (optionalLot.isPresent()) {
                Lot lot = optionalLot.get();

                Assertions.assertEquals(lot.getName(), newLot.getName());
                Assertions.assertNotNull(lot.getOwner());
                Assertions.assertEquals(lot.getOwner(), user);
                Assertions.assertEquals(lot.getInstantBuyPrice(), lot.getInstantBuyPrice());
                Assertions.assertEquals(lot.getInitialPrice(), lot.getInitialPrice());
                Assertions.assertEquals(lot.getDescription(), lot.getDescription());
            } else {
                Assertions.fail("The lot hadn't been created");
            }
            System.out.println(contentAsString);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            List<Lot> lotList = lotRepository.findByOwnerId(user.getId());
            lotRepository.deleteAll(lotList);
            userRepository.delete(user);
            auctionRepository.delete(auction);
        }
    }
}
