# Auction
Auction is REST web service application that provides possibilities to create and manage your own auction.

## Quick start
- Clone the repository.
- Run the application by typing in command line `mvnw spring-boot:run`
####
- To run test simply use `mvnw test`

## Documentation
You can find Auction documentation by running the server and go to the page http://localhost:8080/api/swagger-ui/#/.

## Additional
This application doesn't have any security protection. 
Instead, it simply checks header property email in each API request. 
This property should contain an email of the user and checks whether the user with the given email exists in the database. 
If not, the server will throw an exception. So user creation requests should be going first before using any other(except some) API's.